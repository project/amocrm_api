<?php

/**
 * @file
 * Administrative page callbacks for the amocrm_api module.
 */

/**
 * General configuration form for controlling the amocrm_api behaviour.
 */
function amocrm_api_admin_settings() {
  $custom_fields = variable_get('amocrm_api_custom_fields', array());

  $form['amocrm_api_default_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Default domain'),
    '#default_value' => variable_get('amocrm_api_default_domain'),
    '#description' => t('amoCRM general settings page: "Settings->General settings". "Subdomain" field.'),
  );

  $form['amocrm_api_default_email'] = array(
    '#type' => 'textfield',
    '#title' => t('Default email'),
    '#default_value' => variable_get('amocrm_api_default_email'),
    '#description' => t('Email of the user who owns the account.'),
  );

  $form['amocrm_api_default_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Default api key'),
    '#default_value' => variable_get('amocrm_api_default_key'),
    '#description' => t('amoCRM api settings page: "Settings->API". "Your API Key" field.'),
  );

  if (!empty($custom_fields)) {
    $variable_name = 'amocrm_api_custom_fields';
    $custom_fields_name = variable_get('amocrm_api_custom_fields_name', array());
    $form[$variable_name] = array(
      '#type' => 'fieldset',
      '#title' => t('Custom fields'),
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    foreach ($custom_fields as $key => $field) {
      if (!empty($field)) {
        $form[$variable_name][$key] = array(
          '#type' => 'fieldset',
          '#title' => t('Custom field: "@key"', array('@key' => ucfirst($key))),
          '#tree' => TRUE,
          '#collapsible' => TRUE,
          '#collapsed' => TRUE,
        );
        foreach ($field as $code => $id) {
          $form[$variable_name][$key][$code] = array(
            '#type' => 'textfield',
            '#title' => $custom_fields_name[$id] . ' [' . $code . ']',
            '#default_value' => $id,
            '#disabled' => TRUE,
          );
        }
      }
    }
  }

  return system_settings_form($form);
}

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function amocrm_api_form_amocrm_api_admin_settings_alter(&$form, &$form_state, $form_id) {
  $form['#submit'][] = 'amocrm_api_admin_settings_submit';
}

/**
 * Submit callback for amocrm-api-admin-settings form.
 */
function amocrm_api_admin_settings_submit($form, &$form_state) {
  $custom_fields = array();
  $custom_fields_name = array();
  $values = $form_state['values'];
  $client = array(
    $values['amocrm_api_default_domain'],
    $values['amocrm_api_default_email'],
    $values['amocrm_api_default_key'],
  );
  list($domain, $email, $key) = $client;

  if ($domain && $email && $key) {
    try {
      $client = amocrm_api_client($domain, $email, $key);
      $account = amocrm_api_account_load($client);
      foreach ($account['custom_fields'] as $key => $field) {
        foreach ($field as $item) {
          $custom_fields[$key][$item['code']] = $item['id'];
          $custom_fields_name[$item['id']] = $item['name'];
        }
      }
    }
    catch (Exception $e) {
      watchdog_exception('amoCRM API', $e);
    }
  }

  variable_set('amocrm_api_custom_fields', $custom_fields);
  variable_set('amocrm_api_custom_fields_name', $custom_fields_name);
}
